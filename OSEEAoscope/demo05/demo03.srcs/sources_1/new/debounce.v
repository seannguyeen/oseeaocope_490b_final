`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// OSEEAoscope: FPGA Digital Oscilloscope  
// CECS 490B Fall 2020
// 
// Engineers: Sean Nguyen, Edward Chorro, Alec Velazquez, and Ethan Nguyen 
// 
// File Name: debounce.v    
// 
// Description: Debounce Buttons from input
//////////////////////////////////////////////////////////////////////////////////
module debounce #(parameter DELAY=270000)   // .01 sec with a 27Mhz clock
	        (input reset, clk, noisy,
	         output reg clean);

   reg [18:0] count;
   reg new;

   always @(posedge clk)
     if (reset)
       begin
	  count <= 0;
	  new <= noisy;
	  clean <= noisy;
       end
     else if (noisy != new)
       begin
	  new <= noisy;
	  count <= 0;
       end
     else if (count == DELAY)
       clean <= new;
     else
       count <= count+1;
      
endmodule

						 
