`timescale 1ns / 1ps
/****************************** C E C S  3 0 1 ******************************
* File Name:	pixel_controller.v
* Project:		Lab Project 7: CPU Execution Unit
* Designer: 	Chorro, Edward / Velazquez, Alec
* Email: 		edwardchorro@gmail.com / velazquez_alec@yahoo.com
* Rev. Date: 	April 24, 2018 
* 
* Purpose: 		The purpose of this module is to generate the signals for the
*					common anode inputs to the 7-segment displays. Furthermore, the
*					module also generates the multiplexor select signals for 
*					multiplexing the address/data nibbles. This is achieved by 
*					creating two case statements for the next state (combo logic) 
*					and another for the output (combo logic). The sequential logic
*					is controlled by the state register. The seg_sel output will 	
*					be fed into the 8-1 MUX. At each state there are eleven bits
*					that are outputed. Furthermore, the state machine has no inputs
*					besides clk and reset. This indicates that the state machine is
* 					always changing states. 
* 
* Notes: 		All 8 anodes are not on at once, rather each one is on 
*					indepedently ON and are being refreshed at such a quick rate 
*					that it looks as if all of the anodes are on at once. We are 
*					turning on the far left anode (anode7) first then the last anode 
*					(anode0) is being turned on last. Lighting the anodes from left 
*					to right. That could be seen the the output combinational logic. 
*					To turn on an anode you assign a 0. To turn off an anode, you 
*					assign it with a 1. 
*****************************************************************************/
module pixel_controller(clk, reset, anode, seg_sel, dp);

	input 	clk, reset; 
	output 	[7:0] anode;
	output 	[2:0] seg_sel;
	output dp;
	reg		[7:0] anode;
	reg		[2:0] seg_sel;
	reg dp; 
	
	//*********************
	// state register
	// next_state variables
	//*********************
	
	reg [2:0] present_state, next_state;
	
	//***********************
	// Next State Combo Logic
	// **********************
	
	always @( present_state ) 
		case ( present_state ) 
				3'b000: next_state = 3'b001; 
				3'b001: next_state = 3'b010; 
				3'b010: next_state = 3'b011; 
				3'b011: next_state = 3'b100; 
				3'b100: next_state = 3'b101; 
				3'b101: next_state = 3'b110;
				3'b110: next_state = 3'b111; 
				3'b111: next_state = 3'b000;
			  default: next_state = 3'b000; 
		endcase
		
	
	//****************************************
	// State Register Logic (Sequential Logic)
	// ***************************************
	
	always @(posedge clk or posedge reset) 
		if (reset == 1'b1) 
			present_state = 3'b000;
			//Got a reset, so set register to all 0s
		else 
			present_state = next_state;
			//Got a posedge so update state register with next state
		
	//******************************
	// Output Combo Logic (LT to RT)
	// *****************************
	
	always @( present_state ) 
		case ( present_state ) 
				3'b000: {anode, seg_sel, dp} = 12'b01111111_000_1; // ANODE 7 ON
				3'b001: {anode, seg_sel, dp} = 12'b10111111_001_1; // ANODE 6 ON
				3'b010: {anode, seg_sel, dp} = 12'b11011111_010_1; // ANODE 5 ON
				3'b011: {anode, seg_sel, dp} = 12'b11101111_011_1; // ANODE 4 ON 
				3'b100: {anode, seg_sel, dp} = 12'b11110111_100_0; // ANODE 3 ON
				3'b101: {anode, seg_sel, dp} = 12'b11111011_101_1; // ANODE 2 ON
				3'b110: {anode, seg_sel, dp} = 12'b11111101_110_1; // ANODE 1 ON
				3'b111: {anode, seg_sel, dp} = 12'b11111110_111_1; // ANODE 0 ON 
			   default: {anode, seg_sel, dp} = 12'b01111111_000_1; // ANODE 7 ON
		endcase
		
endmodule


