`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// OSEEAoscope: FPGA Digital Oscilloscope  
// CECS 490B Fall 2020
// 
// Engineers: Sean Nguyen, Edward Chorro, Alec Velazquez, and Ethan Nguyen 
// 
// File Name: cursor_text
// 
// Description: This module takes in binary values as an input, converts it to decimal
// and returns the address of the corresponding character
//////////////////////////////////////////////////////////////////////////////////
module cursor_text
    #(parameter DISPLAY_Y_BITS = 12,
                SCALE_EXPONENT_BITS = 4,
                SCALE_FACTOR_SIZE = 10,
                SELECT_CHARACTER_BITS = 7,
                VOLTAGE_BITS = 12,
                DIGIT_BITS = 4)
    (
    input clock,
    input signed [DISPLAY_Y_BITS-1:0]signal,
    input [SCALE_EXPONENT_BITS-1:0] scaleExponent,
    input [SCALE_FACTOR_SIZE-1:0] scale,
    output [SELECT_CHARACTER_BITS-1:0] character2,
    output [SELECT_CHARACTER_BITS-1:0] character1,
    output [SELECT_CHARACTER_BITS-1:0] character0,
    output isPositive
    );
    
    // CURSOR TO VOLTAGE
    wire signed [VOLTAGE_BITS-1:0] voltage;
    wire [VOLTAGE_BITS-1:0] voltageAbsoluteValue;
    wire voltageIsNegative;
    
    cursor_to_voltage CURSOR2VOLT (.clock(clock),
                                   .y(signal),
                                   .scaleExponent(scaleExponent),
                                   .scale(scale),
                                   .voltage(voltage),
                                   .voltageAbsoluteValue(voltageAbsoluteValue),
                                   .isNegative(voltageIsNegative));
    
    // CONVERT VOLTAGE TO 3-BIT DECIMAL          
    binary2decimal BIN2DEC(.clock(clock),
                          .data(voltageAbsoluteValue),
                          .character0(character0),
                          .character1(character1),
                          .character2(character2));
          
    assign isPositive = ~voltageIsNegative;
    
endmodule

