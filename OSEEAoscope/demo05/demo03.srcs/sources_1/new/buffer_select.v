`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// OSEEAoscope: FPGA Digital Oscilloscope  
// CECS 490B Fall 2020
// 
// Engineers: Sean Nguyen, Edward Chorro, Alec Velazquez, and Ethan Nguyen 
// 
// File Name: buffer_select.v
// 
// Description: Everytime the VGA reaches the bottom right most pixel, toggel which buffer is being used
//////////////////////////////////////////////////////////////////////////////////
module buffer_select(

    input clock,
    input drawStarting,
    output reg activeBramSelect
    );
    
    always @(posedge clock) begin
        if (drawStarting)
            activeBramSelect <= ~activeBramSelect;
    end
endmodule
