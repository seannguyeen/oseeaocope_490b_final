// Copyright 1986-2018 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2018.2 (win64) Build 2258646 Thu Jun 14 20:03:12 MDT 2018
// Date        : Sun Nov 22 23:50:24 2020
// Host        : DESKTOP-PNS2FA1 running 64-bit major release  (build 9200)
// Command     : write_verilog -force -mode synth_stub
//               C:/temp/DEMOS/oseeocope/demo05/demo05/demo03.runs/CharactersROM_synth_1/CharactersROM_stub.v
// Design      : CharactersROM
// Purpose     : Stub declaration of top-level module interface
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------

// This empty module with port declaration file causes synthesis tools to infer a black box for IP.
// The synthesis directives are for Synopsys Synplify support to prevent IO buffer insertion.
// Please paste the declaration into a Verilog source file or add the file as an additional source.
(* x_core_info = "blk_mem_gen_v8_4_1,Vivado 2018.2" *)
module CharactersROM(clka, ena, addra, douta, clkb, enb, addrb, doutb)
/* synthesis syn_black_box black_box_pad_pin="clka,ena,addra[16:0],douta[0:0],clkb,enb,addrb[16:0],doutb[0:0]" */;
  input clka;
  input ena;
  input [16:0]addra;
  output [0:0]douta;
  input clkb;
  input enb;
  input [16:0]addrb;
  output [0:0]doutb;
endmodule
